﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BSK_Zadaniaa.Algorithm;
namespace BSK_Zadaniaa.View
{
    /// <summary>
    /// Logika interakcji dla klasy Zadanie1.xaml
    /// </summary>
    public partial class Zadanie1 : UserControl
    {
        public string []names {get;set;}
       

        private Button LoadFilePassword;
        private Button LoadFileKey;

        private TextBox Password;
        private TextBox Key;


        //zmienne do okreslenia ustawień
        private int FromFile = 0;


        public Zadanie1()
        {
            InitializeComponent();
            names = new string[] { "zadanie 1", "zadanie 2", "zadanie 3" };

            DataContext = this;

            ButtonObj.Click += Encrypt_Button;
            ButtonDes.Click += Decrypt_Button;

            CheckBoxe.Click += box_checked;

            PasswordText.Visibility = Visibility.Visible;
            KeyText.Visibility = Visibility.Visible;

        }


        private void box_checked(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("Hello, world!", "My App");
            if (FromFile == 0)
            {
                
                FromFile = 1;

                PasswordText.Visibility = Visibility.Hidden;
                KeyText.Visibility = Visibility.Hidden;

                ButtonLoadPassword.Visibility = Visibility.Visible;
                ButtonLoadKey.Visibility = Visibility.Visible;


            }
            else
            {

                ButtonLoadPassword.Visibility = Visibility.Hidden;
                ButtonLoadKey.Visibility = Visibility.Hidden;

                PasswordText.Visibility = Visibility.Visible;
                KeyText.Visibility = Visibility.Visible;

              
                FromFile = 0;
            }
           
        }


        private void Encrypt_Button(object sender, RoutedEventArgs e)
        {
            string textToEncrypt="", keyText = "", outText = "";
            int valid = 0;

            if (FromFile == 0)
            {
                //odczyt z okienek

                if(PasswordText.Text != "" && KeyText.Text != "")
                {

                textToEncrypt = PasswordText.Text;
                keyText = KeyText.Text;
                    valid = 1;
                }
                else
                {
                    ((MainWindow)System.Windows.Application.Current.MainWindow).OpenMessage("Uzupełnij poprawnie pola!");
                }
                

            }
            else
            {
                //odczyt z pliku
            }






            if (ComboValues.Text == "zadanie 1" && valid==1 )
            {
                RailFence RailFence = new RailFence();

                outText = RailFence.RailFenceEncryption(textToEncrypt,int.Parse(keyText));

                Solution.Text = outText; 
            } 
            else if(ComboValues.Text == "zadanie 2" && valid == 1)
            {
                //zadanie2
                outText = textToEncrypt + " " + keyText + ComboValues.Text;
                Solution.Text = outText; //tutaj wstawić stringa z wynikiem odszyfrowywania

            }
            else if(ComboValues.Text == "zadanie 3" && valid == 1)
            {

                //Zadanie3

                Example3 algorytm3 = new Example3();


                outText = algorytm3.Encrypt(textToEncrypt, keyText);

                Solution.Text = outText; //tutaj wstawić stringa z wynikiem odszyfrowywania
            }
            else if(valid==1)
            {
                
                ((MainWindow)System.Windows.Application.Current.MainWindow).OpenMessage("Musisz wybrać algorytm!");
            }

           
        }

        private void Decrypt_Button(object sender, RoutedEventArgs e)
        {


            string textToEncrypt = "", keyText = "", outText = "";
            int valid = 0;

            if (FromFile == 0)
            {
                //odczyt z okienek

                if (PasswordText.Text != "" && KeyText.Text != "")
                {

                    textToEncrypt = PasswordText.Text;
                    keyText = KeyText.Text;
                    valid = 1;
                }
                else
                {
                    Solution.Text = "Uzupelnij poprawnie pola!";
                }


            }
            else
            {
                //odczyt z pliku
            }






            if (ComboValues.Text == "zadanie 1" && valid == 1)
            {
                RailFence RailFence = new RailFence();

                outText = RailFence.RailFenceDescription(textToEncrypt, int.Parse(keyText));

                Solution.Text = outText;
            }
            else if (ComboValues.Text == "zadanie 2" && valid == 1)
            {
                //zadanie2
                outText = textToEncrypt + " " + keyText + ComboValues.Text;

                Solution.Text = outText; //tutaj wstawić stringa z wynikiem odszyfrowywania

            }
            else if (ComboValues.Text == "zadanie 3" && valid == 1)
            {

                //Zadanie3

                outText = textToEncrypt + " " + keyText + ComboValues.Text;

                Solution.Text = outText; //tutaj wstawić stringa z wynikiem odszyfrowywania
            }
            else if (valid == 1)
            {
                Solution.Text = "Wybierz Algorytm";
            }
        }

        private void ButtonDes_Click(object sender, RoutedEventArgs e)
        {
            string textToEncrypt = "", keyText = "", outText = "";
            int valid = 0;

            if (FromFile == 0)
            {
                //odczyt z okienek

                if (PasswordText.Text != "" && KeyText.Text != "")
                {

                    textToEncrypt = PasswordText.Text;
                    keyText = KeyText.Text;
                    valid = 1;
                }
                else
                {
                    ((MainWindow)System.Windows.Application.Current.MainWindow).OpenMessage("Uzupełnij poprawnie pola!");
                }


            }
            else
            {
                //odczyt z pliku
            }






            if (ComboValues.Text == "zadanie 1" && valid == 1)
            {
                //Zadanie1
                outText = textToEncrypt + " " + keyText + ComboValues.Text;

                RailFence RailFence = new RailFence();

                outText = RailFence.RailFenceDescription(textToEncrypt, int.Parse(keyText));


                Solution.Text = outText; //tutaj wstawić stringa z wynikiem odszyfrowywania

            }
            else if (ComboValues.Text == "zadanie 2" && valid == 1)
            {
                //zadanie2
                outText = textToEncrypt + " " + keyText + ComboValues.Text;
                Solution.Text = outText; //tutaj wstawić stringa z wynikiem odszyfrowywania

            }
            else if (ComboValues.Text == "zadanie 3" && valid == 1)
            {

                //Zadanie3

                Example3 algorytm3 = new Example3();


                outText = algorytm3.Encrypt(textToEncrypt, keyText);

                Solution.Text = outText; //tutaj wstawić stringa z wynikiem odszyfrowywania
            }
            else if (valid == 1)
            {

                ((MainWindow)System.Windows.Application.Current.MainWindow).OpenMessage("Musisz wybrać algorytm!");
            }
        }
    }
}
