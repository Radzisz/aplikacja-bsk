﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BSK_Zadaniaa.ViewModel;
namespace BSK_Zadaniaa
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

          

        }

        private void Zadanie1_Start(object sender, MouseButtonEventArgs e)
        {

            DataContext = new Zadanie1ViewModelcs();
        }

        private void Zadanie2_Start(object sender, MouseButtonEventArgs e)
        {
            DataContext = new Zadanie2ViewModel();
        }


        public void OpenMessage(string text)
        {
            MessageBox.Show(text, "My App");
        }
    }

}
